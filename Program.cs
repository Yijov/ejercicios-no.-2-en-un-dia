﻿using System;

namespace Ejercicios_No._2___En_un_dia
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            mayorNum();

            Console.ReadKey();
        }
        //devuelve valor absoluto
        static void AbsoluteVal(){
            try
            {
                Console.Write("por favor inserte un número ");
            var AbsVal = Convert.ToDouble(Console.ReadLine()); 
            if (AbsVal>0){
                Console.Write(AbsVal);
            } else {
                Console.Write(AbsVal*-1);
            }
                
            }
            catch (System.Exception)
            {  
             Console.Write("El valor introducido no es un numero valido");
             AbsoluteVal();
            }
            
        }

        // verifica el menor de dos números
        static void mayorNum () {
            try
            {
                Console.Write("inserte el primer número ");
                double firstNum = Convert.ToDouble(Console.ReadLine()); 
                Console.Write("inserte el segundo número ");
                double secondNum = Convert.ToDouble(Console.ReadLine()); 
                if (firstNum > secondNum) {
              Console.WriteLine( firstNum + " es el mayor");
              } else if (firstNum < secondNum) {
                  Console.WriteLine(secondNum + " es el mayor");
              } else  {
                  Console.WriteLine("Ambos numeros son iguales");

              }
        
            }
            catch (System.Exception)
            {
                
                Console.WriteLine("Por favor introduzca numeros validos");
                mayorNum();
            }
            
        }

        static void ejercicio4() {
        Console.WriteLine("presione un caracter");
        char character = Convert.ToChar(Console.ReadKey()); 
        character = Char.ToLower(character);
        
        switch (character)
        {
            case 'a':
            case 'e': 
            case 'i': 
            case 'o':
            case 'u':
            Console.WriteLine("Es una vocal");
            break;
            case '1':
            case '2': 
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            Console.WriteLine("Es un número");
            break;
            default:
            Console.WriteLine("Es un símbolo");
            break;
        }
    }


        static void ordenDescendente(){
            int num = 22;
            while (num >=7 )
            {
                Console.WriteLine(num);
                num = num-2;
            }

        }
        
    }
}
